﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class PuckControl : MonoBehaviour {

	public AudioClip wallCollideClip;
	private AudioSource audio;
	public AudioClip paddleCollideClip;
	public LayerMask paddleLayer;

	public Transform startingPos;
	private Rigidbody rigidbody;

	// Use this for initialization
	void Start () {
		audio = GetComponent<AudioSource> ();
		rigidbody = GetComponent<Rigidbody>();
		ResetPosition();
	}

	public void ResetPosition() {
		// teleport to the starting position
		rigidbody.MovePosition(startingPos.position);
		// stop it from moving
		rigidbody.velocity = Vector3.zero;
	}


	void OnCollisionEnter(Collision collision) {
	//	Debug.Log("Collision Enter" + collision.gameObject.name);
	//}

	//void OnCollisionStay(Collision collision) {
		//Debug.Log("Collision Stay" + collision.gameObject.name);
	//}

	//void OnCollisionExit(Collision collision) {
		//Debug.Log("Collision Exit" + collision.gameObject.name);
	//}
		// check what we have hit
		if (paddleLayer.Contains(collision.gameObject)) {
			// hit the paddle
			audio.PlayOneShot(paddleCollideClip);
		}
		else {
			// hit something else
		audio.PlayOneShot(wallCollideClip);
}
}
}

